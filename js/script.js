const API = "https://ajax.test-danit.com/api/swapi/films/";
const root = document.querySelector("#root");
const renderFilm = (url) => {
    fetch(url)
        .then(response => response.json())
        .then(data => {

            const films = document.createElement("div")
            films.classList.add("films")

            data.forEach(({ characters, episodeId, name, openingCrawl }) => {
                let charsArr = []

                const list = document.createElement("ul")
                list.classList.add("chars-list")

                const filmCard = document.createElement("div")
                filmCard.classList.add("film")

                characters.forEach(elem => {
                    charsArr.push(fetch(elem)
                        .then(response => response.json()))
                })
                Promise.all(charsArr)
                    .then(charsArr => {
                        charsArr.forEach(({ name }) => {
                            chars = name
                           list.insertAdjacentHTML("beforeend", `<li class="chars-list__item">${chars}</li> `)
                        });
                    })

                filmCard.insertAdjacentHTML("beforeend", `
                    <h2 class="film__title">${name}</h2>
                    <p class="film__episode">Episode: ${episodeId}</p>
                    <p class="film__desc"> ${openingCrawl}</p>
                    `)

                filmCard.append(list)
                films.append(filmCard)
                root.append(films)
            })
        })
}

renderFilm(API)




